package com.mat.way.dependencybetweenviews;

import android.app.Activity;
import android.os.Bundle;
import android.util.Log;

public class MainActivity extends Activity {

    public static final String TAG = "MainActivity";

    private TrimmingController mTrim;
    private SeekingController mSeek;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        mTrim = (TrimmingController) findViewById(R.id.trim);
        mSeek = (SeekingController) findViewById(R.id.seek);

        //setting callback to trimming view. Here you can handle events from trimmer.
        mTrim.setCallback(new TrimmingController.Callback() {
            @Override
            public void onLeftSlide() {
                Log.d(TAG, "onLeftSlide");
            }

            @Override
            public void onRightSlide() {
                Log.d(TAG, "onRightSlide");
            }

            @Override
            public void onRelease() {
                Log.d(TAG, "onRelease");
            }
        });
    }

}
