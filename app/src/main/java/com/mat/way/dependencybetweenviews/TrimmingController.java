package com.mat.way.dependencybetweenviews;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.util.AttributeSet;
import android.view.MotionEvent;
import android.view.View;

/**
 * Created by MPODOLSKY on 07.07.2015.
 */
public class TrimmingController extends View implements View.OnTouchListener{

    public static final int LEFT_SLIDER = 1;
    public static final int RIGHT_SLIDER = 2;
    public static final int NONE = 0;

    private float   leftPosition,
                    rightPosition,
                    leftShift,
                    rightShift;
    private int     mWidth,
                    mHeight,
                    mCurrentSlider,
                    mSliderWidth;
    private Paint   mShadow, mSliderPaint;
    private Callback mCallback;

    public interface Callback {
        void onLeftSlide();
        void onRightSlide();
        void onRelease();
    }

    public TrimmingController(Context context) {
        super(context);
        this.setOnTouchListener(this);
    }

    public TrimmingController(Context context, AttributeSet attrs) {
        super(context, attrs);
        this.setOnTouchListener(this);
    }

    public void init(){
        initializeVariables();
    }

    @Override
    protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
        super.onMeasure(widthMeasureSpec, heightMeasureSpec);
        mWidth = getMeasuredWidth();
        mHeight = getMeasuredHeight();
        init();
    }

    @Override
    protected void onDraw(Canvas canvas) {
        super.onDraw(canvas);
        //left slider
        canvas.drawRect(leftPosition, 0, leftPosition + mSliderWidth, mHeight, mSliderPaint);
        //right slider
        canvas.drawRect(rightPosition, 0, rightPosition + mSliderWidth, mHeight, mSliderPaint);
        //shadow near left slider
        canvas.drawRect(0, 0, leftPosition, mHeight, mShadow);
        //shadow near right slider
        canvas.drawRect(rightPosition + mSliderWidth, 0, mWidth, mHeight, mShadow);
    }

    @Override
    public boolean onTouch(View view, MotionEvent event) {
        switch (event.getAction()) {
            case MotionEvent.ACTION_DOWN:
                checkTouchCollision(event);
                break;
            case MotionEvent.ACTION_MOVE:
                moveSlider(event);
                break;
            case MotionEvent.ACTION_UP:
                clearShift();
                if (mCallback != null) {
                    mCallback.onRelease();
                }
                mCurrentSlider = NONE;
                break;
            default:
                break;
        }
        return true;
    }

    private void initializeVariables(){
        leftPosition = 0;
        mSliderWidth = 60;
        rightPosition = mWidth - mSliderWidth;
        mShadow = new Paint();
        mShadow.setColor(Color.parseColor("#33000000"));
        mSliderPaint = new Paint();
        mSliderPaint.setColor(Color.parseColor("#88AAAA"));
    }

    public void setCallback(Callback callback) {
        this.mCallback = callback;
    }

    private void checkTouchCollision(MotionEvent event) {
        float x = event.getX();
        if (x >= leftPosition && x <= leftPosition + mSliderWidth) {
            mCurrentSlider = LEFT_SLIDER;
            leftShift = x - leftPosition;
        } else if (x >= rightPosition && x <= rightPosition + mSliderWidth) {
            mCurrentSlider = RIGHT_SLIDER;
            rightShift = x - rightPosition;
        }
    }

    private void clearShift(){
        leftShift = 0;
        rightShift = 0;
    }

    private void moveSlider(MotionEvent event) {
        switch (mCurrentSlider) {
            case LEFT_SLIDER:
                if ((int)event.getX() - leftShift < 0) {
                    leftPosition = 0;
                } else {
                    if ((int) event.getX() - leftShift + mSliderWidth >= rightPosition) {
                        leftPosition = rightPosition - mSliderWidth;
                    } else {
                        leftPosition = (int) event.getX() - leftShift;
                    }
                }
                if (mCallback != null) {
                    mCallback.onLeftSlide();
                }
                invalidate();
                break;
            case RIGHT_SLIDER:
                if ((int)event.getX() - rightShift > mWidth - mSliderWidth) {
                    rightPosition = mWidth - mSliderWidth;
                } else {
                    if ((int) event.getX() - rightShift <= leftPosition + mSliderWidth) {
                        rightPosition = leftPosition + mSliderWidth;
                    } else {
                        rightPosition = (int)event.getX() - rightShift;
                    }
                }
                if (mCallback != null) {
                    mCallback.onRightSlide();
                }
                invalidate();
                break;
            case NONE:
                break;
        }
    }
}
