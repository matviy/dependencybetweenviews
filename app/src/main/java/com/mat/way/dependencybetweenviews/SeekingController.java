package com.mat.way.dependencybetweenviews;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.util.AttributeSet;
import android.view.MotionEvent;
import android.view.View;

/**
 * Created by MPODOLSKY on 07.07.2015.
 */
public class SeekingController extends View implements View.OnTouchListener{

    private float   position,//current slider position
                    shift;  //distance between touch position and slider left border
    private int     mWidth, //view full width
                    mHeight,//view full height
                    mSliderWidth;
    private boolean isSelected; //this variable shows if the slider was touched. If the slider is touched we won't send the touch event further
    private Paint   mSliderPaint;

    public SeekingController(Context context) {
        super(context);
        this.setOnTouchListener(this);
    }

    public SeekingController(Context context, AttributeSet attrs) {
        super(context, attrs);
        this.setOnTouchListener(this);
    }

    public void init(){
        initializeVariables();
    }

    @Override
    protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
        super.onMeasure(widthMeasureSpec, heightMeasureSpec);
        mWidth = getMeasuredWidth();
        mHeight = getMeasuredHeight();
        init();
    }

    @Override
    protected void onDraw(Canvas canvas) {
        super.onDraw(canvas);
        canvas.drawRect(position, 0, position + mSliderWidth, mHeight, mSliderPaint);
    }

    private void initializeVariables(){
        mSliderWidth = 60;
        position = mWidth / 2 - mSliderWidth / 2;
        mSliderPaint = new Paint();
        mSliderPaint.setColor(Color.parseColor("#FF0000"));
    }

    @Override
    public boolean onTouch(View view, MotionEvent event) {
        switch (event.getAction()) {
            case MotionEvent.ACTION_DOWN:
                checkTouchCollision(event);
                break;
            case MotionEvent.ACTION_MOVE:
                moveSlider(event);
                break;
            case MotionEvent.ACTION_UP:
                clearShift();
                isSelected = false;
                return true;
            default:
                break;
        }
        return isSelected;
    }

    private void checkTouchCollision(MotionEvent event) {
        //check if touch coordinates inside slider
        float x = event.getX();
        if (x >= position && x <= position + mSliderWidth) {
            shift = x - position;
            isSelected = true;
        }
    }

    private void clearShift(){
        shift = 0;
    }

    private void moveSlider(MotionEvent event) {
        //change position of slider and redraw view
        if (isSelected) {
            if ((int)event.getX() - shift < 0) {
                position = 0;
            } else if ((int)event.getX() - shift > mWidth - mSliderWidth) {
                position = mWidth - mSliderWidth;
            } else {
                position = (int)event.getX() - shift;
            }
            invalidate();
        }
    }
}
